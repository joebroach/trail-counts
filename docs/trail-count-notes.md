---
title: trail-count notes
author: joe.broach@oregonmetro.gov
date: 2019-08-20
---
Updates:
[Oct 28 2019 meeting notes](meeting-2018-10-28.html)

# Two goals
## Expand 2-hr counts to AADNT
Primary need: best estimate of Average Annual Daily Non-motorized Traffic
(AADNT) at locations that only have short-duration counts (SDCs).

Important features:

* easy to maintain and update for future years
* consistent across count years for comparisons
* incorporate weather effects

Key outcome: best state-of-the-practice expansion factors to estimate
AADNT given SDCs and regional continuous counters.

In the past: consultant factored 2014-2015 (?) SDCs using a combination of
the TMG/AASHTO method and dissaggregate factor (DF) method.

* TMG uses averaged month-of-year, day-of-week, and hour-of-day factors from
similar "Factor group" continuous count sites.
* DF uses a simple day-of-year factor from comparable site(s); e.g. an SDC on
Sep 9th is expanded based on the ratio of Sep 9th to AADNT at one or more
continuous count sites.  

DF has the advantage of accounting for weather and other local areas effects
on a specific day.

Proposed method: Try Disaggregate Factoring (DF) with TMG as fallback
(or as comparison?). Define factor groups for continuous counters by hourly
distributions. Assign factor groups by same trail/area(?) and some
qualitative judgment. Could potentially use intercept survey data or bike/ped
ratios to help.

Considerations:

* Factor groups: there's no formal method for defining factor groups, and it's
especially tricky with SDCs <24 hrs, since no time of day pattern is available.
Guidance suggests minimum of 3-5 continuous counters per factor group to avoid
potential major errors. Typical groupings are based on "peakiness" and
weekend/weekday ratios. Usually no more than 3-4 factor groups.

## Adjust yearly totals for weather
Primary need: Even if AADNT estimates were 100% accurate, there's still a
desire to adjust bike and ped volumes to account for weather differences from
year to year.

Important features:
* maintain comparability (common base year or weather factors)
* need to update for future years

Key outcome: Weather-adjusted AADNT (or just ANT)--*what we would have
expected in an "average" weather year.*

Proposed method: Apply UMn weather models for "Marine" climate to adjust daily
counts, then aggregate up to create weather adjusted AADNT and/or ANT. Could
potentially re-estimate to get Portland-specific coefficients.

Considerations: Separate equations for bike/ped, so would need to estimate
splits. This is an "experimental" application. Need to define base weather
pattern (past 10?).

# Details
## Data
* Short-duration counts
  * 2009-2018
  * weekday
  * AM or PM peak 2-hr
  * 36 "indicator" sites
* Continuous counters (Fig 1)
  * Trafx
    * 2011(?)-present
    * mostly combined bike/ped (some bike only?)
    * hourly resolution
    * limited geography
  * PBOT
    * 13 arterial/non-bridge
    * bike only (?)
    * Moody Cycletrack
    * Hawthorne Brg
  * ODOT  
    * 2018, need to ask about earlier data
    * mostly bike, some bike-ped
    * ~15 counters, most with separate directional counts

![Counter locations](images/counters-all.png)
Figure 1: Known continuous counter locations

## Methods
### Expansion factors
### Weather adjustment
