# Read and apply basic QC checks to bike data
# 
# Logic provided by Krista Nordback, based on her research
#
# Note: one change was made to the processing logic due to the network
#   focus of this analysis. Directions are not aggregated by counter, since
#   network links have two directions.
#
# Data prep
#
# Count data needs to be in .csv files either in the base directory itself 
# or else in sub-folders of the base directory.
# 
# Each count data file should contain ONLY bike counts data in 2-3 columns:
#   starttime - a datetime string in LOCAL time, ISO formatted
#   nb[|wb|eb|sb] - directional bike count, first direction
#   nb[|wb|eb|sb] - [optional] directional bike count, second direction
# Any additional fields will be ignored
#
# 

# include trailing slash in base_dir
base_dir = "C:/Users/broachj/workspaces/trail-counts/data/Boulder (2018 data)/13th _ Walnut/"
data_dir_level = 0  # 0 for base, 1 for one deep, etc.
tz_name = "America/Denver"  ## timezone name 
                            ## (https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) 
#gdrive = TRUE  ## switch to handle google drive name mangling
prefix_remove = "bike_counters_"  # any filename prefix to ignore 

file_list = list()
# crawl the folders
i = 1
if (data_dir_level == 0) {
  for (f in list.files(base_dir)) {
    if (endsWith(f, '.csv')) {
      file_list[[i]] <- paste0(base_dir, f)
      i = i + 1
    }
  } 
}

for (f in file_list) {
  df <- read.csv(f)
  print(f)
  print(summary(df))
  
  df$starttime <- strptime(df$starttime, 
                           format="%m/%d/%Y  %I:%M:%S %p",
                           tz=tz_name)
  
}
head(df)

strptime("7/25/2014  2:45:00 AM", 
         format="%m/%d/%Y  %I:%M:%S %p",
         tz=tz_name)

for (dir0 in list.files(base_dir)) {
  print(dir0)
  for (dir1 in list.files(paste0(base_dir, dir0)))
       print(dir1)
}


#str_detect()