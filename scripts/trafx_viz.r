# Quick viz of permanent count sites
rm(list=ls())
library(ggplot2)
library(dplyr)
#library(readr)
library(tidyr)
library(pacman)
p_load(ggforce)
library(leaflet)
p_load(plotly)
p_load(htmlwidgets)

# include trailing slash in base_dir
base_dir = "I:/Trail-Counts/trafx/raw/"
data_dir_level = 0  # 0 for base, 1 for one deep, etc.
max_zero_time <- as.difftime(1, units="weeks")
max_hourly = 1500
max_daily = 15000
#tz_name = "America/Los Angeles"  ## timezone name 
## (https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) 
#gdrive = TRUE  ## switch to handle google drive name mangling
#prefix_remove = "bike_counters_"  # any filename prefix to ignore 
suffix_remove = " RAW"  # any suffix (before .csv) to ignore
file_list = list()
# crawl the folders
i = 1
for (f in list.files(base_dir)) {
  if (endsWith(f, '.csv')) {
    file_list[[i]] <- f
    i = i + 1
  }
} 

count_df <- data.frame("site"=character(0), "start_time"=character(0), 
                       "count"=numeric(0), stringsAsFactors=F)
for (f in file_list) {
  print(paste("Reading", f, "..."))
  df <- read.csv(paste0(base_dir, f), header=F, stringsAsFactors=F)
  colnames(df) <- c("start_time", "count")
  df$site <- substr(f, 1, nchar(f) - 4 - nchar(suffix_remove))  # TODO make this smarter
  df$date <- as.Date(substr(df$start_time, 1, 10))
  print(paste(min(substr(df$start_time, 1, 4)), "to",
              max(substr(df$start_time, 1, 4))))
  print(summary(df$count))
  count_df <- bind_rows(count_df, df)
}
print("Converting to calendar times...")
count_df$start_time <- as.POSIXct(count_df$start_time,
                                  format="%Y-%m-%d %H:%M:%S",
                                  tz="America/Los_Angeles")  # time-
# Trafx records a count at 2am on "leap forward" day, which technically doesn't exist!
count_df <- count_df[-which(is.na(count_df$start_time)),]

#count_df[is.na(count_df$start_time), "date"]
count_df$ID <- c(1:nrow(count_df))
head(count_df)
#summary(count_df)
#table(count_df$site)

# load some site data
sites <- read.csv("I:/Trail-Counts/trafx/siteinfo.csv", stringsAsFactors=F)
head(sites)
sites$X <- NULL  # some sort of artifact?
table(sites$Site.name)
count_df <- merge(count_df, sites, by.x="site", by.y="Site.name")
head(count_df)

# Map the sites
sites[sites$Site.name %in% count_df$site,] %>% leaflet() %>%
  addProviderTiles(providers$OpenStreetMap) %>% 
  addTiles() %>%  
  addMarkers(~Longitude, ~Latitude, 
             popup = ~Site.name)

# tally daily counts
count_df.daily <- count_df %>% 
  group_by(site, date) %>% 
  summarize(sum = sum(count), n = n()) %>%
  as.data.frame()

# basic qc cleanup
# (1) remove days with more than 24h of counts
count_df$ncount_FLG <- FALSE
for (site in unique(count_df.daily$site)) {
  bad_days <- count_df.daily[count_df.daily$site==site & 
                             count_df.daily$n > 24, "date"]
  print(paste(site, ":", length(bad_days), "bad days will be flagged"))
  count_df[count_df$site==site & 
             count_df$date %in% bad_days, "ncount_FLG"] <- TRUE
  
}

count_df <- count_df[-which(count_df$ncount_FLG),]

# (2) remove excess zeroes
print(paste("Checking for zero count periods longer than", 
            format(max_zero_time)))

count_df$zero_FLG <- FALSE
for (site in unique(count_df$site)) {
  zero_span = c(NA, NA)
  #site_index <- which(count_df$site==site)
  for (ii in which(count_df$site==site)) {
    if (count_df[ii, "count"] == 0) {
      if (all(is.na(zero_span))) {
        zero_span <- c(count_df[ii, "start_time"],
                       count_df[ii, "start_time"])
      }
      else {
        zero_span[2] <- count_df[ii, "start_time"]
      } 
    }
    else if (!all(is.na(zero_span)) & 
             zero_span[2] - zero_span[1] > max_zero_time) {
      count_df[count_df$site==site & 
               count_df$start_time>=zero_span[1] & 
               count_df$start_time<zero_span[2], "zero_FLG"] <- TRUE
      zero_span <- c(NA, NA)
    }
    else {zero_span <- c(NA, NA)}
    #print(zero_span)
    #if (!all(is.na(zero_span))) {
    #  print(paste(count_df[ii, "date"], format(zero_span[2] - zero_span[1])))
    #}
  }
  # handle last set of rows
  if (!all(is.na(zero_span)) & zero_span[2] - zero_span[1] > max_zero_time) {
    count_df[count_df$site==site & 
             count_df$start_time>=zero_span[1] & 
             count_df$start_time<zero_span[2], "zero_FLG"] <- TRUE
  }
  print(paste(site, ":", nrow(count_df[count_df$site==site & 
                                         count_df$zero_FLG == TRUE, ]), 
              "hourly counts flagged"))
}

count_df <- count_df[-which(count_df$zero_FLG == TRUE), ]

summary(count_df.daily)
# RE-tally daily counts
count_df.daily <- count_df %>% 
  group_by(site, date) %>% 
  summarize(sum = sum(count), n = n()) %>%
  as.data.frame()
summary(count_df.daily)

# (3) suspiciously high counts
count_df$hourly_FLG <- FALSE
for (site in unique(count_df$site)) {
  #site_index <- which(count_df$site==site)
  count_df[count_df$site==site & count_df$count > max_hourly, 
           "hourly_FLG"] <- TRUE
  print(paste(site, ":", nrow(count_df[count_df$site==site & 
                                         count_df$hourly_FLG == TRUE, ]), 
              "hourly counts flagged"))
  }
summary(count_df)
count_df <- count_df[-which(count_df$hourly_FLG == TRUE), ]

summary(count_df.daily)
# RE-tally daily counts
count_df.daily <- count_df %>% 
  group_by(site, date) %>% 
  summarize(sum = sum(count), n = n()) %>%
  as.data.frame()
summary(count_df.daily)

count_df.daily$daily_FLG <- FALSE
count_df$daily_FLG <- FALSE
for (site in unique(count_df.daily$site)) {
  #site_index <- which(count_df$site==site)
  count_df.daily[count_df.daily$site==site & count_df.daily$sum > max_daily, 
           "daily_FLG"] <- TRUE
  print(paste(site, ":", nrow(count_df.daily[count_df.daily$site==site & 
                                         count_df.daily$daily_FLG == TRUE, ]), 
              "daily counts flagged"))
  for (date in count_df.daily[count_df.daily$site==site & 
                              count_df.daily$daily_FLG == TRUE, "date"]) {
    count_df[count_df$site==site & count_df$date==date, "daily_FLG"] <- TRUE
  }
}
count_df <- count_df[-which(count_df$daily_FLG == TRUE), ]


summary(count_df.daily)
# RE-tally daily counts
count_df.daily <- count_df %>% 
  group_by(site, date) %>% 
  summarize(sum = sum(count), n = n()) %>%
  as.data.frame()
summary(count_df.daily)


# output as paginated pdfs
#a <- a[order(a$City, a$link_name, a$link_direction, a$Date), ]
#counters <- unique(a[c("City", "link_name", "link_direction")])
#counters <- counters[order(counters$City, counters$link_name, 
#                           counters$link_direction), ]

# fill in date gaps for plotting
as.Date("2019-12-31") - as.Date("2009-01-01")
span <- data.frame(date=seq(ISOdate(2009,1,1), by = "day", length.out = 4016))
span$date <- as.Date(span$date)

count_df.daily <- count_df.daily %>% 
  complete(nesting(site),
           date=span) %>% as.data.frame()
                            
counters <- unique(count_df.daily$site)
pdf("outputs/trafx-counts-test.pdf")
for (p in (1:5)) {
  i = p * 5 - 4
  j = p * 5
  #counts <- merge(count_df.daily, counters[i: j], by)
  counts <- count_df.daily[count_df.daily$site %in% counters[i: j], ]
  print(p <- ggplot(counts,
                    aes(x=date,
                        y=sum, group=1)) +
          geom_line(color="blue") +
          geom_vline(xintercept=seq(as.Date(0, origin="2009-01-01"),
                         length=11, by="1 year"), linetype=4, color="salmon",
                     lwd=0.2) +
          facet_grid_paginate(vars(site),
                              scales="free",
                              page = i, labeller = label_wrap_gen(width=10)) + 
          theme(axis.text = element_text( size = 8),
                strip.text = element_text(size = 6))
  )
  
}
dev.off()
###

#+
#  geom_vline(aes(xintercept=as.numeric(as.Date("2018-09-01"))))

d <- data.frame("x"=c(1,1,2), "y"=c(2,4,6))
d
d[-which(d$x==1),]

zero_span <- c(as.POSIXct("2017-11-18 16:00:00 PST"), as.POSIXct("2017-11-29 13:00:00 PST"))


###
# charts
#install.packages('BiocManager')
#library(BiocManager)
library(candela)
#install.packages('devtools')
#devtools::install_github('Kitware/candela', subdir='R/candela', dependencies = TRUE)

data <- list(
  list(name='Do this blah blah blah blah', level=1, start=0, end=5),
  list(name='This part 1', level=2, start=0, end=3),
  list(name='This part 1', level=2, start=5, end=7),
  list(name='This part 2', level=2, start=3, end=5),
  list(name='Then that', level=1, start=5, end=15),
  list(name='That part 1', level=2, start=5, end=10),
  list(name='That part 2', level=2, start=10, end=15))

candela('GanttChart',
        data=data, label='name',
        start='start', end='end', level='level',
        width=700, height=200)


### Plotly test ###
  #counts <- merge(count_df.daily, counters[i: j], by)
#counts <- count_df.daily[count_df.daily$site %in% counters[i: j], ]
p <- ggplot(count_df.daily,
          aes(x=date,
              y=sum, group=1)) +
geom_line(color="blue", lwd=0.1) +
geom_vline(xintercept=as.numeric(seq(as.Date(0, origin="2009-01-01"),
                          length=11, by="1 year")), linetype=4, color="salmon",
           lwd=0.2) +
facet_wrap(vars(site), ncol = 3,
                    scales="free") + 
theme(axis.text = element_text( size = 10),
      strip.text = element_text(size = 8))
p
ggplotly(p)
saveWidget(ggplotly(p), file = "trafx-counters.html")               
